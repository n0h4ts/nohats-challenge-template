# Challenge Proposal

## Sources / Inspiration
```List down any links for your source or inspiration, helps to provide context to the reviewer```
## Challenge Title
```fill in a creative challenge title```
## Challenge Category
```challenge category, can be one or many or the closest that you can think of```
## Challenge Description / Question
```fill in question pr description of challenge```
## Challenge Difficulty
```What should be the difficulty of this challenge? Easy / Medium / Hard```
## Challenge Hints
```If a hint should be given out, what would that hint be?```
## Challenge Flag
CYBERLEAGUE{```fill_in_flag_here```}
## Challenge Resources
```What resource is required to deploy this challenge? e.g. web challenges require deployment on server. In that case the challenge needs to be containerized. If the challenge is of steganograpy nature then only uploading of the file will be required```
