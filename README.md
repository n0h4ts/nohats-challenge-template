# N0H4TS Challenge Template

Challenge template for submissions

## Challenge Guidelines
1. Challenge submitted must be related to Infosecurity or Cybersecurity topics such as, but not limited to:
    - Web Exploitations
    - Forensics
    - Reverse Engineering
    - Binary Exploitation
    - Cryptography
    - Mobile Security
    - Internet of Things
    - Steganography
    - Programming
    - Open Source Intelligence (OSINT)
2. Challenge need not be limited to one topic.
3. Level of difficulty of the challenge can be ranked as Easy, Medium or Hard.
4. Challenge should be solvable within reasonable time (1-5 or 24 Hours), according to the challenge rank.
5. Challenges should not encourage brute forcing or require probing of servers. If brute forcing is required for a challenge, the plaintext should be easily found in common wordlists. E.g. rockyou.txt
6. If challenges have the need to be deployed, it is encouraged for the challenge to be containerized either using docker or docker-compose.
7. OSINT Challenge should not require or suggest contact with any real life person.

## Submission Requirements
1. [Challenge proposal](PROPOSAL.md) (Should follow proposal template provided, in markdown).
1. Challenge Question or description (Would be visible to player, this is included in proposal template).
2. Any challenge files required (Would be visible to player).
3. Challenge source code (includes docker or project management files e.g. docker, maven, gradle scripts)
4. Documentation on dependencies and how to build application (e.g. Makefile, shell scripts, requirements.txt files).
5. [Challenge solution](WRITEUP.md) (writeup of how to solve the challenge, submit in markdown template or PDF).
6. Challenge flag (Flag will be reviewed or changed if required).
7. [T&C Document](Terms_And_Conditions.md) Provide your name and email address in the terms and condition document.

## Criteria for challenge submission
1. Solvable within reasonable time according to challenge rank. (Time stated below is just a soft guideline) 
    - Easy, 1-3 Hours
    - Medium, 1-5 Hours
    - Hard, 4-24 Hours
2. Challenge should not require any probing of servers. For OSINT challenges, it should not require or suggest contact with any real life person.
3. Challenge should meet challenge guidelines and requirements stated above.
