# Terms and Conditions of Challenge Creation Bounty for Cyber League 2021/2022

Thank you for your interest in submitting a challenge for Cyber League 2021/2022! It is important for you to read this document carefully to make sure that you understand and agree to the Terms and Conditions stated below. Please add your signature at the end of this document and submit it with your challenge. For any clarifications, please email us at support@cyberleague.co. 

By submitting your challenge, you hereby agree to the following Terms and Conditions:

**1. Eligibility** <br>
    1.1. This challenge creation is open to anyone. <br>
    1.2. You can only submit **1 (ONE)** challenge per round. <br>
    1.3. The submitted challenge must be original and created by you. It must not have been previously published or used in any form in Singapore or anywhere else in the world, or entered in any other competition. <br>
    1.4. This document must be signed and submitted with your Challenge.<br>
    1.5. Email your challenge (with this form) to support@cyberleague.co. <br>
    1.6. If your challenge is selected for use during Cyber League 2021/2022, you will receive the incentive. You may still participate in Cyber League 2021/2022 but **your team will not be awarded points for the submitted challenge**.<br>

**2. Confidentiality**<br>
    2.1 You hereby declare that you have not disclosed the submitted challenge and/or its solution(s) (in whole or in part, including any drafts or iterations thereof) to any other person. <br>
    2.2 You hereby agree to keep the details of the submitted challenge and its solution(s) and not disclose the same to any person until the end of Cyber League 2021/2022. <br>

**3. Intelletual Property**<br>
    3.1. In the event that your submitted challenge is selected for the use during Cyber League 2021/2022, you grant to Cyber League an exclusive, unrestricted, royalty-free, world-wide, perpetual and irrevocable license to use your submitted challenge, the relevant solution(s), and any modifications or calibrations of the submitted challenge and relevant solution(s) for the purposes of Cyber League 2021/2022.<br>
    3.2 Cyber League will conduct a test on the selected challenge before deploying it to Cyber League 2021/2022. You hereby grant Cyber League to modify and/or calibrate your challenge (if needed) at any time during or after the testing. <br>
    3.3 Your consent will be sought for Cyber League's use of the challenge beyong Cyber League 2021/2022. <br>
    3.4. You will be acknowledged as the creator of the challenge. <br>

**4. You declare that:**<br>
    4.1. There are no legal or other obligation(s) which prevents you from submitting your challenge and authorising Cyber League's use of the same for the purposes of Cyber League 2021/2022. <br>
    4.2. You are the sole owner of your challenge, including but not limited to the intellectual property rights in the same; <br>
    4.3. No third party has any right, title, claim, or interest in your challenge; and <br>
    4.4. Your challenge does not violate or infringe any copyright, trademark or other intellectual property rights of any person or entity, and does not violate or infringe on the moral rights, rights of privacy other rights of any person or entity. <br>
    4.5. You undertake to indemnify and hold Cyber League, its team harmless against any and all liabilities, losses, damages, claims, injuries, actions, proceedings, expenses, and cost arising from the submission of your challenge. <br>
    4.6. Cyber League's decision on all matters relating to or in connection to Cyber League 2021/2022 and this challenge creation shall be final and binding. Cyber League is not obliged to give any reason or explanations as to why a challenge is selected or rejected.<br> 


**_Acknowledgement_**

I acknowledge that I have read all the points in this document and agree to all of the Terms and Conditions set out above. I further acknowledge that any breach of the above Terms and Conditions may cause me to forfeit my incentive (if any). I acknowledge that by submitting this challenge I have agreed to the Terms and Conditions set out above.


_Name_: 

_Email Address_:
